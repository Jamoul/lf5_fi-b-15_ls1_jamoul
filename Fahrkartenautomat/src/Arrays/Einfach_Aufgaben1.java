package Arrays;

/**
 * @author jamoul
 *
 * @category Einfache �bungen zu Arrays Aufgabe 1
 * 
 * @description Schreiben Sie ein Programm �Zahlen�, in welchem ein ganzzahliges Array der L�nge 10
 * deklariert wird. Anschlie�end wird das Array mittels Schleife mit den Zahlen von 0 bis 9
 * gef�llt. Zum Schluss geben Sie die Elemente des Arrays wiederum mit einer Schleife auf der
 * Konsole aus.
 * 
 */
public class Einfach_Aufgaben1 {
	
	/**
	 * @param args
	 */
	public static void main(String[] args) {

		int[] zahlenfeld = new int[10];

		for (int i = 0; i < zahlenfeld.length; i++) {
			zahlenfeld[i] = i;
		}

		for (int i = 0; i < zahlenfeld.length; i++) {
			System.out.println(zahlenfeld[i]);
		}

	}
}
