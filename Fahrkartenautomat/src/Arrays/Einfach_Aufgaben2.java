package Arrays;

/**
 * @author jamoul
 *
 * @category Einfache �bungen zu Arrays Aufgabe 2
 * 
 * @description Das zu schreibende Programm �UngeradeZahlen� ist �hnlich der Aufgabe 1. Sie deklarieren
 * wiederum ein Array mit 10 Ganzzahlen. Danach f�llen Sie es mit den ungeraden Zahlen von 1 bis 19
 * und geben den Inhalt des Arrays �ber die Konsole aus (Verwenden Sie Schleifen!).
 * 
 */
public class Einfach_Aufgaben2 {

	/**
	 * @param args
	 */
	public static void main(String[] args) {

		int[] zahlenfeld = new int[10];
		int x = 1;
		for (int i = 0; i < zahlenfeld.length; i++) {

			zahlenfeld[i] = x;
			x = x + 2;

		}

		for (int i = 0; i < zahlenfeld.length; i++) {
			System.out.println(zahlenfeld[i]);
		}

	}
}
