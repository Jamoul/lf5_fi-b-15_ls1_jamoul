package Arrays;

import java.util.Scanner;

/**
 * @author jamoul
 *
 * @category Einfache �bungen zu Arrays Aufgabe 3
 * 
 * @description Aufgabe 3 Im Programm �Palindrom� werden �ber die Tastatur 5
 *              Zeichen eingelesen und in einem geeigneten Array gespeichert.
 *              Ist dies geschehen, wird der Arrayinhalt in umgekehrter
 *              Reihenfolge (also von hinten nach vorn) auf der Konsole
 *              ausgegeben.
 * 
 */
public class Einfach_Aufgaben3 {

	/**
	 * @param args
	 */
	public static void main(String[] args) {

		Scanner scanner = new Scanner(System.in);

		String charEingabe = scanner.nextLine();
		
		if(charEingabe.toCharArray().length == 5) {

		char[] charEingabeArray = new char[5];
		charEingabeArray = charEingabe.toCharArray();

		for (int i = charEingabeArray.length - 1; i >= 0; i--) {
			System.out.println(charEingabeArray[i]);
		}
		
		
		}else {

			System.err.println("Fatal Error: Geben sie bitte nur 5 zeichen ein");
		}

		scanner.close();

	}

}
