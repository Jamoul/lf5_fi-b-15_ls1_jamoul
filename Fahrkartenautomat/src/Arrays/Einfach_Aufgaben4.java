package Arrays;

import java.util.Scanner;

/**
 * @author jamoul
 *
 * @category Einfache �bungen zu Arrays Aufgabe 4
 * 
 * @description Jetzt wird Lotto gespielt. In der Klasse �Lotto� gibt es ein
 *              ganzzahliges Array, welches 6 Lottozahlen von 1 bis 49 aufnehmen
 *              kann. Konkret sind das die Zahlen 3, 7, 12, 18, 37 und 42.
 *              Tragen Sie diese im Quellcode fest ein.
 * 
 */
public class Einfach_Aufgaben4 {

	/**
	 * @param args
	 */
	public static void main(String[] args) {

		// int[] zahlenfeld = new int[10];

		Scanner scanner = new Scanner(System.in);
		int eingabe = scanner.nextInt();

		int[] zahlenfeld = { 3, 7, 12, 18, 37, 42 };

		for (int i = 0; i < zahlenfeld.length; i++) {
			if (eingabe == zahlenfeld[i]) {
				System.out.printf("Die Zahl %d ist in der Ziehung enthalten.", zahlenfeld[i]);
				break;
			} else {
				System.out.printf("Die Zahl %d ist nicht in der Ziehung enthalten.", eingabe);

			}

		}

	}
}
