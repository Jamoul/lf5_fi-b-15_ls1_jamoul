import java.util.Scanner;

class FahrkartenautomatMitWarteUndMuezeAusgeben {

	static Scanner tastatur = new Scanner(System.in);

	public static double fahrkartenbestellungErfassen() {

		double zuZahlenderBetrag;
		/** new code **/
		int TicketsAnzahl;

		System.out.print("Ticketpreis (EURO): ");
		zuZahlenderBetrag = tastatur.nextDouble();

		/** new code **/
		System.out.print("Anzahl der Tickets: ");
		TicketsAnzahl = tastatur.nextInt();

		return zuZahlenderBetrag * TicketsAnzahl;
	}

	public static double fahrkartenBezahlen(double zuZahlen) {

		double eingezahlterGesamtbetrag;
		double eingeworfeneMaenze;
		// Geldeinwurf
		// -----------
		eingezahlterGesamtbetrag = 0.0;

		while (eingezahlterGesamtbetrag < zuZahlen) {

			System.out.printf("Noch zu zahlen: %10.2f Euro%n", zuZahlen - eingezahlterGesamtbetrag);
			System.out.print("Eingabe (mind. 5Ct, hoechstens 2 Euro): ");
			eingeworfeneMaenze = tastatur.nextDouble();
			eingezahlterGesamtbetrag += eingeworfeneMaenze;
		}

		return eingezahlterGesamtbetrag;
	}

	public static void fahrkartenAusgeben() {

		System.out.println("\nFahrschein wird ausgegeben");

		for (int i = 0; i < 8; i++) {
			System.out.print("=");
			warte(250);
		}

		System.out.println("\n\n");
	}

	public static void rueckgeldAusgeben(double eingezahlterGesamtbetrag, double zuZahlenderBetrag) {

		double rueckgabebetrag = eingezahlterGesamtbetrag - zuZahlenderBetrag;

		if (rueckgabebetrag > 0.0) {
			System.out.println("Der rueckgabebetrag in Hoehe von " + rueckgabebetrag + " EURO");
			System.out.println("wird in folgenden Maenzen ausgezahlt:");

			while (rueckgabebetrag >= 2.0) // 2 EURO-Maenzen
			{
				muenzeAusgeben(2, "EURO");
				rueckgabebetrag -= 2.0;
			}
			while (rueckgabebetrag >= 1.0) // 1 EURO-Maenzen
			{
				muenzeAusgeben(1, "EURO");
				rueckgabebetrag -= 1.0;
			}
			while (rueckgabebetrag >= 0.5) // 50 CENT-Maenzen
			{
				muenzeAusgeben(50, "CENT");
				rueckgabebetrag -= 0.5;
			}
			while (rueckgabebetrag >= 0.2) // 20 CENT-Maenzen
			{
				muenzeAusgeben(20, "CENT");
				rueckgabebetrag -= 0.2;
			}
			while (rueckgabebetrag >= 0.1) // 10 CENT-Maenzen
			{
				muenzeAusgeben(10, "CENT");
				rueckgabebetrag -= 0.1;
			}
			while (rueckgabebetrag >= 0.05)// 5 CENT-Maenzen
			{
				muenzeAusgeben(5, "CENT");
				rueckgabebetrag -= 0.05;
			}
		}

		System.out.println("\nVergessen Sie nicht, den Fahrschein\n" + "vor Fahrtantritt entwerten zu lassen!\n"
				+ "Wir wuenschen Ihnen eine gute Fahrt.");
	}

	public static void warte(int millisekunde) {
		try {
			Thread.sleep(250);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
	}

	public static void muenzeAusgeben(int betrag, String einheit) {

		System.out.printf("%d %s%n", betrag, einheit);
	}

	public static void main(String[] args) {

		double zuZahlen = fahrkartenbestellungErfassen();

		// Geldeinwurf
		// -----------
		double eingezahlterGesamtbetrag = fahrkartenBezahlen(zuZahlen);

		// Fahrscheinausgabe
		// -----------------
		fahrkartenAusgeben();

		// Rueckgeldberechnung und -Ausgabe
		// -------------------------------
		rueckgeldAusgeben(eingezahlterGesamtbetrag, zuZahlen);
	}
}