package Schleifen;
/**
 * @category Aufgaben Schleifen 1 Aufgabe 4: Folgen
 * @author user
 *
 */
public class AB4_4_Schleifen_1_For {

	public static void main(String[] args) {
		folge_a();
		folge_b();
		folge_c();
		folge_d();
		folge_e();
	} 
	/**
	 * folge_a
	 */
	public static void folge_a() {
		System.out.print("a) ");
		for(int i= 99; i>=9;i-=3) {
			
			System.out.print(i+", ");
		}
		System.out.print("\n");
	} 
	/**
	 * folge_b
	 */
	public static void folge_b() {
		System.out.print("b) ");
		for(int i= 1; i<400;i++) {
			
			System.out.print(i+", ");
		}
		System.out.print("\n");
	}
	/**
	 * folge_c
	 */
	public static void folge_c() {
		System.out.print("c) ");
		for(int i= 2; i<=102;i+=4) {
			
			System.out.print(i+", ");
		}
		System.out.print("\n");
	}
	/**
	 * folge_d
	 */
	public static void folge_d() {
		System.out.print("d) ");
		for(int i= 2; i<=32; i=i+2 ) {
			
			System.out.print(i*i+", ");
		}
		System.out.print("\n");
	}
	/*
	 * folge_e
	 */
	public static void folge_e() {
		System.out.print("e) ");
		for(int i= 2; i<=32768;i*=2) {
			
			System.out.print(i+", ");
		}
		System.out.print("\n");
	}

}
