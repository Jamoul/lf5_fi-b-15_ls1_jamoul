package Schleifen;

/**
 * @category Aufgaben Schleifen 2 Aufgabe 2: Fakult�t
 * 
 * @author jamoul
 */
public class AB4_4_Schleifen_2_While2 {

	public static void main(String[] args) {
		int eingabe = 10;
		fackultaet(eingabe);
	}

	/**
	 * @category fackultaet
	 */
	public static void fackultaet(int eingabe) {
		System.out.print(eingabe + "! =");

		int fakultaet = 1;
		int lauf = 1;

		while (lauf <= eingabe) {
			System.out.print(lauf + "*");
			fakultaet = fakultaet * lauf;
			lauf++;
			if (lauf == eingabe) {
				System.out.print(lauf + "= " + fakultaet);
				break;
			}
		}
		System.out.println("\nEgebnis: " + fakultaet);
	}

}
