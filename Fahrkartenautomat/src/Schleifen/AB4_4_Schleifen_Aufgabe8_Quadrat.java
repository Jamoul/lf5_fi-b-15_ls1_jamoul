package Schleifen;

import java.util.Scanner;

/**
 * 
 * @category Aufgaben zu Schleifen 1 Aufgabe 8: Quadrat
 * 
 * @author Jamoul
 *
 */
public class AB4_4_Schleifen_Aufgabe8_Quadrat {

	public static void main(String[] args) {

		System.out.println("Bitte geben sie die Seitenlšnge des Quadrats ein:");
		
		Scanner scanner = new Scanner(System.in);
		int seitenlanger = scanner.nextInt();
		
		baueQuatrad(seitenlanger);
		scanner.close();

	}
	
	/**
	 * @name baueQuatrad
	 * 
	 * @param seitenlanger
	 */
	public static void baueQuatrad(int seitenlanger) { 
		for (int i = 0; i < seitenlanger; i++) {
			System.out.printf("%s ", "*");
		}

		for (int i = 2; i < seitenlanger; i++) {
			System.out.printf("%n*%" + (seitenlanger * 2 - 2) + "s", "*");
		}

		System.out.print("\n");
		
		for (int i = 0; i < seitenlanger; i++) {
			System.out.printf("%s ", "*");
		}

	}

}
