/**
 * 
 */
package Schleifen;

import java.util.Scanner;

/**
 * @author jamoul
 * @category AB Schleifen 1 Aufgabe 6
 */
public class AB_Schleifen_1_For_Stern_Aufgabe6 {

	/**
	 * @param args
	 */
	public static void main(String[] args) {

		Scanner scanner = new Scanner(System.in);
		System.out.println("Geben Sie die Anzahl der Sterne ein:");

		int stenEingabe = scanner.nextInt();

		BauTreppe(stenEingabe);

		scanner.close();

	}
	
	/**
	 * @param stenEingabe
	 */
	public static void BauTreppe(int stenEingabe) {

		for (int x = 0; x < stenEingabe; x++) {
			if (x == 0) {
				System.out.print("");
			} else {
				System.out.print("\n");
			}
			for (int i = 0; i <= x; i++) {
				System.out.print("*");
			}
		}

	}

}
