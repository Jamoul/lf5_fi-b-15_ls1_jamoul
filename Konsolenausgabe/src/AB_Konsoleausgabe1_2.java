
public class AB_Konsoleausgabe1_2 {  
	
	public static void main(String[] args) {
	// Baum version 1
	String[] stars = {"*","***","*****",
					"*******","*********","***********",
					"*************","***","***"};  
	
	
	System.out.printf("%7s%n %7s%n %8s%n", stars[0],stars[1],stars[2]);
	System.out.printf("%10s%n %10s%n %10s%n", stars[3],stars[4],stars[5]); 
	System.out.printf("%10s%n %7s%n %7s%n", stars[6],stars[7],stars[8]); 
	
	// Baum version 2
	String starForTree2 = 
			  "      *       \r\n"
			+ "     ***      \r\n"
			+ "    *****     \r\n"
			+ "   *******    \r\n"
			+ "  *********   \r\n"
			+ " ***********  \r\n"
			+ "************* \r\n"
			+ "     ***      \r\n"
			+ "     ***";  


System.out.printf("%n%s%n", starForTree2);

	
	
	
	}


}
