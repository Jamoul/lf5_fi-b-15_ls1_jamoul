/*
 * F�nf (Double) Type Zahlen  werden 
 * mit Hilfe der printf()-Anweisung formatiert.
*/ 
public class AB_Konsoleausgabe1_3 {  
	
	public static void main(String[] args) {
	
	double ersteNummer = 22.4234234;
	double zweiteNummer = 111.2222;
	double dritteNummer = 4.0;
	double vierteNummer = 1000000.551;
	double f�nfteNummer = 97.34;

	System.out.printf("%1.2f%n", ersteNummer); 
	System.out.printf("%.2f%n", zweiteNummer); 
	System.out.printf("%1.2f%n", dritteNummer);
	System.out.printf("%.2f%n", vierteNummer);
	System.out.printf("%f%n", f�nfteNummer);
		
	}

}
