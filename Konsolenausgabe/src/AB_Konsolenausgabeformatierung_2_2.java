
public class AB_Konsolenausgabeformatierung_2_2 {

	public static void main(String[] args) {
		String n = "1 * 2 * 3 * 4 * 5"; 
		String gleichSymbol= "=";
		System.out.printf("%-5s%-2s%19.0s%s%4s%n","0!",gleichSymbol,n,gleichSymbol,"1");
		System.out.printf("%-5s%-2s%-19.2s%s%4s%n","1!",gleichSymbol,n,gleichSymbol,"1");
		System.out.printf("%-5s%-2s%-19.6s%s%4s%n","2!",gleichSymbol,n,gleichSymbol,"2");
		System.out.printf("%-5s%-2s%-19.9s%s%4s%n","3!",gleichSymbol,n,gleichSymbol,"6"); 
		System.out.printf("%-5s%-2s%-19.14s%s%4s%n","4!",gleichSymbol,n,gleichSymbol,"24");
		System.out.printf("%-5s%-2s%-19s%s%4s%n","5!",gleichSymbol,n,gleichSymbol,"120");

	}

}
