
public class AB_Konsolenausgabeformatierung_2_3 {

	public static void main(String[] args) {
		
		String pipe = "|";
		System.out.printf("%-12s%s%10s%n","Fahrenheit",pipe, "Celsius");
		System.out.printf("%.23s%n", "------------------------------------");
		System.out.printf("%+-12d%-5s%-+1.2f%n",-20,pipe,-28.89);
		System.out.printf("%+-12d%-5s%-+1.2f%n",-10,pipe,-23.33); 
		System.out.printf("%+-12d%-5s%-+1.2f%n",0,pipe,-17.78);
		System.out.printf("%+-12d%-6s%-+1.2f%n",20,pipe,-6.67);
		System.out.printf("%+-12d%-6s%-+1.2f%n",30,pipe,-1.11);

	}

}
