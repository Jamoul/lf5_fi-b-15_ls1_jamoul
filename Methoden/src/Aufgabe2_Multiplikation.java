
public class Aufgabe2_Multiplikation {

	public static void main(String[] args) {

		double wert1 = 2.36;
		double wert2 = 12.2;

		multiplizieren(wert1, wert2);

		wert1 = 9.4;
		wert2 = 7.8;
		multiplizieren(wert1, wert2);

		wert1 = 12.3;
		wert2 = 87.2;
		multiplizieren(wert1, wert2);
	}

	public static void multiplizieren(double zahl1, double zahl2) {

		double mutiplikationsErgebnis = zahl1 * zahl2;

		System.out.println("Ergebnis: "+ mutiplikationsErgebnis);
	}

}
