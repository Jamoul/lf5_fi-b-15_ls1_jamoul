import java.util.Scanner;

public class Aufgabe3_4_Volumenberechnung {
	/**
	 * a) Würfel: V = a * a * a b) Quader: V = a * b * c c) Pyramide: V = a * a * h
	 * / 3 d) Kugel: V = 4/3 * r³ * π.
	 */

	public static void main(String[] args) {
		Scanner scanner = new Scanner(System.in);

		double ergebnis = wuerfel(scanner);
		ausgabe("Wuerfel", ergebnis);
		
		ergebnis = quader(scanner);
		ausgabe("Quader", ergebnis);
		
		ergebnis = pyramide(scanner);
		ausgabe("Pyramide", ergebnis);
		
		ergebnis = kugel(scanner);
		ausgabe("Kugel", ergebnis); 
	

	}

	public static double wuerfel(Scanner scanner) {
		System.out.println("bitte geben sie die Wuerfel (A) ein:");
		double a = scanner.nextDouble();

		return a * a * a;

	}

	public static double quader(Scanner scanner) {
		System.out.println("bitte geben sie die Quader Messung (A) ein:");
		double a = scanner.nextDouble();

		System.out.println("bitte geben sie die Quader Messung (B) ein:");
		double b = scanner.nextDouble();

		System.out.println("bitte geben sie die Quader Messung (C) ein:");
		double c = scanner.nextDouble();

		return a * b * c;
	}

	public static double pyramide(Scanner scanner) {

		System.out.println("bitte geben sie die pyramide Messung (A) ein:");
		double a = scanner.nextDouble();

		System.out.println("bitte geben sie die Quader Messung (H) ein:");
		double h = scanner.nextDouble();

		return a * a * (h / 3);
	}

	public static double kugel(Scanner scanner) {

		System.out.println("bitte geben sie die Kugel Messung (R) ein:");
		double r = scanner.nextDouble();

		return (4 / 3) * (r * r * r) * Math.PI;
	}

	public static void ausgabe(String message, double ergebnis) {
		System.out.printf("%s Volumen: %f%n", message, ergebnis);
	}

}
