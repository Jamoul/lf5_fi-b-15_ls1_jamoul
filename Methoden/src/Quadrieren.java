import java.util.Scanner;

public class Quadrieren {

	public static void main(String[] args) {

		// (E) "Eingabe"
		// Wert f�r x festlegen:
		// ===========================
		title();
		double x = eingabe();

		double ergebnis = verarbeitung(x);
		// (V) Verarbeitung
		// Mittelwert von x und y berechnen:
		// ================================

		// (A) Ausgabe
		// Ergebnis auf der Konsole ausgeben:
		// =================================
		ausgabe(x, ergebnis);

	}

	public static void title() {
		System.out.println("Dieses Programm berechnet die Quadratzahl x�");
		System.out.println("---------------------------------------------");
	}

	public static void ausgabe(double xwert, double erg) {
		System.out.printf("x = %.2f und x�= %.2f\n", xwert, erg);
	}

	public static double eingabe() {
		Scanner tastatur = new Scanner(System.in);
		System.out.println("Bitte eine Zahl eingeben: ");
		double x = tastatur.nextDouble();
		tastatur.close();
		
		return x;
	}

	public static double verarbeitung(double x) {
		double ergebnis = x * x;

		return ergebnis;
	}
}
