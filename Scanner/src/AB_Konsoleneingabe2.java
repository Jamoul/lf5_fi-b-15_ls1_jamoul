
import java.util.Scanner;

public class AB_Konsoleneingabe2 {

	public static void main(String[] args) { 
		
		System.out.println("\nWillkommen!");  
		Scanner myScanner = new Scanner(System.in);
		
		
		System.out.print("\ngebe bitte ihre name:");
		String name = myScanner.nextLine(); 
		
		System.out.print("\ngebe bitte ihre alter:"); 
		int alter = myScanner.nextInt();
		
		System.out.print("\nihre name lautet: "+name);
		System.out.print("\nihre alter lautet: "+alter); 
		
		myScanner.close();
	}

}
