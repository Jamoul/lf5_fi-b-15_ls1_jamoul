﻿/** Variablen.java
    Ergaenzen Sie nach jedem Kommentar jeweils den Quellcode.
    @author hussein jamoul
    @version 1.0.0
*/
public class Variablen {
  public static void main(String [] args){
    /* 1. Ein Zaehler soll die Programmdurchlaeufe zaehlen.
          Vereinbaren Sie eine geeignete Variable */
	  
	  	int Zaehler;
	  	
    /* 2. Weisen Sie dem Zaehler den Wert 25 zu
          und geben Sie ihn auf dem Bildschirm aus.*/
	  	
	  	Zaehler = 25;  
	  	System.out.println("Der Zaehler wert ist: "+ Zaehler);
	  	
    /* 3. Durch die Eingabe eines Buchstabens soll der Zaehler
          eines Programms ausgewaehlt werden.
          Vereinbaren Sie eine geeignete Variable */
	  	
	  	char menuKontrolleBuchstabe;
	  	
    /* 4. Weisen Sie dem Buchstaben den Wert 'C' zu
          und geben Sie ihn auf dem Bildschirm aus.*/
	  	
	  	menuKontrolleBuchstabe = 'C'; 
	  	System.out.println("Der menubuchstabe wert ist: "+menuKontrolleBuchstabe);
	  	
    /* 5. Fuer eine genaue astronomische Berechnung sind grosse Zahlenwerte
          notwendig.
          Vereinbaren Sie eine geeignete Variable */
	  	
	  	long grossZahl;
	  	
    /* 6. Weisen Sie der Zahl den Wert der Lichtgeschwindigkeit zu
          und geben Sie sie auf dem Bildschirm aus.*/
	  	
	  	grossZahl = 300000; 
	  	System.out.println("der Zahl den Wert der Lichtgeschwindigkeit ist: "+grossZahl);
	  	
    /* 7. Sieben Personen gruenden einen Verein. Fuer eine Vereinsverwaltung
          soll die Anzahl der Mitglieder erfasst werden.
          Vereinbaren Sie eine geeignete Variable und initialisieren sie
          diese sinnvoll.*/
	  	
	  	int mitgliederAnzahl = 20;
	  	
    /* 8. Geben Sie die Anzahl der Mitglieder auf dem Bildschirm aus.*/
	  	
	  	System.out.println(" der Zahl der Verein Mitglieder ist: "+mitgliederAnzahl);
	  	
    /* 9. Fuer eine Berechnung wird die elektrische Elementarladung benoetigt.
          Vereinbaren Sie eine geeignete Variable und geben Sie sie auf
          dem Bildschirm aus.*/
	  	
	  	double elektrischeElementarladung = 1.60200000;
	  	System.out.println(" der Berechnung der elektrische Elementarladung ist: "+elektrischeElementarladung);
	  	
    /*10. Ein Buchhaltungsprogramm soll festhalten, ob eine Zahlung erfolgt ist.
          Vereinbaren Sie eine geeignete Variable. */
	  	
	  	boolean istZahlungErfolgt; 
	  	
    /*11. Die Zahlung ist erfolgt.
          Weisen Sie der Variable den entsprechenden Wert zu
          und geben Sie die Variable auf dem Bildschirm aus.*/ 
	  	
	  	istZahlungErfolgt = true;
	  	System.out.println("ist die Zahlung Erfolgt: "+istZahlungErfolgt);

  }//main
}// Variablen